package com.epam.stv.factory.framework.exceptions;

/**
 * Created by Tatiana_Sauchanka on 4/17/2017.
 */
public class UnsupportedDriverTypeException extends RuntimeException {

    private static final long serialVersionUID = -7536364016458937432L;

    public UnsupportedDriverTypeException( String msg )
    {
        super( msg );
    }
}
