package com.epam.stv.factory.framework.framework.core.ui.driver;

import com.epam.stv.factory.framework.exceptions.UnknownDriverTypeException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by Tatiana_Sauchanka on 4/19/2017.
 */
public class Driver {
    private static final String DEFAULT_WEB_DRIVER = "DEFAULT_WEB_DRIVER";

    private static DriverTypes defaultDriverType = DriverTypes.CHROME;

    private static HashMap<String, WebDriver> instances;

    static {
        instances = new HashMap<String, WebDriver>();
    }

    public static WebDriver getWebDriverInstance(String name, DriverTypes type) throws Exception {
        WebDriver driver;
        if (!instances.containsKey(name)) {
            switch (type) {
                case FIREFOX: {
                    driver = new FirefoxDriver();
                    break;
                }
                case IE11: {
                    String exePath = "C:\\IEDriverServer_Win32_3.0.0\\IEDriverServer.exe";
                    System.setProperty("webdriver.ie.driver", exePath);
                    driver = new InternetExplorerDriver();
                    break;
                }
                case CHROME: {
                    String exePath = "C:\\Chromedriver\\chromedriver.exe";
                    System.setProperty("webdriver.chrome.driver", exePath);
                    driver = new ChromeDriver();
                    break;
                }
                default:
                    throw new UnknownDriverTypeException("Unknown web driver specified: " + type);
            }
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            instances.put(name, driver);
        } else {
            driver = instances.get(name);
        }
        return driver;
    }

    public static WebDriver getWebDriverInstance(String name) throws Exception {
        return getWebDriverInstance(name, defaultDriverType);
    }

    public static WebDriver getWebDriverInstance() throws Exception {
        return getWebDriverInstance(DEFAULT_WEB_DRIVER, defaultDriverType);
    }

    public static void setDefaultWebDriverType(DriverTypes type) {
        defaultDriverType = type;
    }
}
