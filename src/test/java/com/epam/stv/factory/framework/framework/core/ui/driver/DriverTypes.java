package com.epam.stv.factory.framework.framework.core.ui.driver;

/**
 * Created by Tatiana_Sauchanka on 4/19/2017.
 */
public enum  DriverTypes {
    FIREFOX, IE11, CHROME;
}
