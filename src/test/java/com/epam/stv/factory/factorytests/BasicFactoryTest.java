package com.epam.stv.factory.factorytests;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.*;
import org.openqa.selenium.chrome.ChromeDriver;


import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by Tatiana_Sauchanka on 2/17/2017.
 */
import static com.epam.stv.locators.LocProjectLocators.START_URL;
import static com.epam.stv.locators.LocProjectLocators.WIGGLE_ICON_LOCATOR;

/**
 * Created by Tatiana_Sauchanka on 2/17/2017.
 */
public class BasicFactoryTest {

    protected static WebDriver driver;

//    private static WebDriver driver;
// use getDriver() for children

    public static WebDriver getDriver() {

        if (driver == null) {
            setChromeDriver();
//            setUpFireFox();
        }
        return driver;
    }

    public static void setChromeDriver()
    {
        String exePath = "C:\\Chromedriver\\chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", exePath);
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setJavascriptEnabled(true);
        driver = new ChromeDriver(capabilities);


    }

    public static void setUpFireFox() {
//        appropriate System Variables have already added

//        String exePath =  "C:\\geckodriver-v0.14.0-win64\\geckodriver.exe";
//        System.setProperty("webdriver.gecko.driver", exePath);

        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        driver = new FirefoxDriver(capabilities);
        capabilities.setJavascriptEnabled(true);

    }

//The steps below were commented due to BDD framework
// Should be uncommented as long as tests from factorytest package are running

//    @BeforeClass(description = "Start browser")
//    public void setUp() {
//        getDriver().get(START_URL);
//        WebElement expectedElement = (new WebDriverWait(driver, 5)).
//                until(new ExpectedCondition<WebElement>() {
//                    public WebElement apply(WebDriver webDriver) {
//                        return webDriver.findElement(WIGGLE_ICON_LOCATOR);
//
//                    }
//                });
//    }
//
//
////    @BeforeClass (dependsOnMethods = "setUp" )
////    public void enableJavaScript(){
////
////    }
//
//    @BeforeClass(dependsOnMethods = "setUp", description = "Add implicite wait and maximize window")
//    public void addImplicitly() {
//        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
//        driver.manage().window().maximize();
//    }

    @AfterMethod
    public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
        if (testResult.getStatus() == ITestResult.FAILURE) {
            File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File("D:\\Lessons\\BDDFramework\\LoggerProject\\test-output\\testScreenShot.jpg"));

        }
    }


    @AfterClass
    public void afterClass() throws Exception {
        driver.quit();

    }

}
