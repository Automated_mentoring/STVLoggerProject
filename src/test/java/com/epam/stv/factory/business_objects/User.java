package com.epam.stv.factory.business_objects;

import static com.epam.stv.locators.LocProjectLocators.USER_EMAIL;
import static com.epam.stv.locators.LocProjectLocators.USER_PASSWORD;

/**
 * Created by Tatiana_Sauchanka on 4/9/2017.
 */
public class User {

    public String getPassword(){
        return USER_PASSWORD;
    }

    public String getEmail(){
        return USER_EMAIL;
    }
}
