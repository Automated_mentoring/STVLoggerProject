Feature: Input credentials from the login page

  Scenario: 01. Required page's field are available
    Given Login page downloaded by default
    Then Password field is available on the Login page

  Scenario Outline: 02. Validate that the validation error appears when the user sets and deletes a password
    Given Password field is available on the Login page
    When The user presses Login in Securely button
    And The user inputs password <password>
    And The user clears password field
    Then Validation error appears
    Examples:
      | password |
      | 4567     |
      | 5678     |







