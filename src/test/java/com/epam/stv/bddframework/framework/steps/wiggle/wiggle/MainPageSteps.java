package com.epam.stv.bddframework.framework.steps.wiggle.wiggle;

import com.epam.stv.factory.factorypages.WiggleMainFactoryPage;
import com.epam.stv.factory.factorypages.WiggleRegisterFactoryPage;
import com.epam.stv.factory.factorypages.WiggleSignInFactoryPage;
import com.epam.stv.factory.factorytests.BasicFactoryTest;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

import static com.epam.stv.locators.LocProjectLocators.MAIN_PAGE;
import static com.epam.stv.locators.LocProjectLocators.REGISTER_URL;

/**
 * Created by Tatiana_Sauchanka on 4/15/2017.
 */
public class MainPageSteps extends BasicFactoryTest{
    @When("^The user clicks on Register link from the Main page$")
    public void clickOnRegisterLink(){
        new WiggleMainFactoryPage(driver).clickOnRegisterLink();
    }

    @When("^The user presses the Basket button from the main page$")
    public void clickOnBasketButton(){
        new WiggleMainFactoryPage(driver).clickOnBasketButton();
    }

    @When("^The user presses Wiggle logo from the Main page$")
    public void clickOnWiggleLogo(){
        new WiggleMainFactoryPage(driver).clickOnWiggleLogo();
    }
//    Open/Restore the page
    @Then("^Main page is (?:opened|restored)$")
    public void assertMainPageIsOpened(){
        String registerURL = new WiggleMainFactoryPage(driver).assertCurrentURL();
        Assert.assertEquals(registerURL,MAIN_PAGE);
    }

    @Then ("^Basket button is displayed on the Main page$")
    public void assertBasketButton() {
        boolean b = new WiggleMainFactoryPage(driver).isBasketButtonDisplayed();
        Assert.assertEquals(b, true);
    }

    @Then ("^Register link is displayed on the Main page$")
    public void assertRegisterLink() {
        boolean b = new WiggleMainFactoryPage(driver).isRegisterLinkDisplayed();
        Assert.assertEquals(b, true);
    }

    @Then ("^Sign link is displayed on the main page$")
    public void assertSignLink() {
        boolean b = new WiggleMainFactoryPage(driver).isSignLinkDisplayed();
        Assert.assertEquals(b, true);
    }

    @Then ("^Basket page opens$")
    public void assertBasketPage(){
        boolean b = new WiggleMainFactoryPage(driver).isContinueShoppingButtonDisplayed();
        Assert.assertEquals(b, true);
    }

    @Then ("^The Register page opens$")
    public void inspectRegisterPage(){
        String registerURL = new WiggleRegisterFactoryPage(driver).assertCurrentURL();
        Assert.assertEquals(registerURL,REGISTER_URL);
    }

}
