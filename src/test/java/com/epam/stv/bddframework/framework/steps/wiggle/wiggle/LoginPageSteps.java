package com.epam.stv.bddframework.framework.steps.wiggle.wiggle;

import com.epam.stv.com.epam.stv.design.WiggleSignInPage;
import com.epam.stv.factory.factorypages.WiggleMainFactoryPage;
import com.epam.stv.factory.factorypages.WiggleSignInFactoryPage;
import com.epam.stv.factory.factorytests.BasicFactoryTest;
import com.epam.stv.loggertesting.LogTest;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static com.epam.stv.locators.LocProjectLocators.START_URL;
import static com.epam.stv.locators.LocProjectLocators.WIGGLE_ICON_LOCATOR;

/**
 * Created by Tatiana_Sauchanka on 4/15/2017.
 */
public class LoginPageSteps extends BasicFactoryTest {

    @Given("^Login page downloaded by default$")
//    need to inspect
    public void setup() throws Throwable {
        getDriver().get(START_URL);
        WebElement expectedElement = (new WebDriverWait(driver, 5)).
                until(new ExpectedCondition<WebElement>() {
                    public WebElement apply(WebDriver webDriver) {
                        return webDriver.findElement(WIGGLE_ICON_LOCATOR);

                    }
                });
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();

    }

    @When("^The user presses Wiggle button from login page$")
    public void clickOnWiggleIcon() {
        new WiggleSignInFactoryPage(driver).clickOnOrangeWiggleIcon();
    }

    @When("^The user presses Login in Securely button$")
    public void clickLoginButton() {
        new WiggleSignInFactoryPage(driver).clickLoginButton();
    }
    @When ("^The user inputs password (\\d+)$")
    public void inputPassword1(int arg){
        new WiggleSignInFactoryPage(driver).inputPassword(arg);
    }

    @When ("^The user clears password field$")
    public void clearPasswordField(){
        new WiggleSignInFactoryPage(driver).clearPassword();
    }

    @Then("^Password field is available on the Login page$")
    public void assertPasswordField(){
        boolean b = new WiggleSignInFactoryPage(driver).isPasswordDisplayed();
        Assert.assertEquals(b, true);
    }

    @Then("^Warning message appears$")
    public void assertWarningAppears() {
        boolean b = new WiggleSignInFactoryPage(driver).isWarningMessageDisplayed();
        Assert.assertEquals(b, true);
    }

    @Then("^Validation error appears$")
    public void assertValidationError() {
        boolean b = new WiggleSignInFactoryPage(driver).isValidationFieldErrorDisplayed();
        Assert.assertEquals(b, true);

    }




}
