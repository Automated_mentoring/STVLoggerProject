package com.epam.stv.grid;

import com.epam.stv.factory.factorypages.WiggleMainFactoryPage;
import com.epam.stv.factory.factorypages.WiggleSignInFactoryPage;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static com.epam.stv.locators.LocProjectLocators.ACCESS_KEY;
import static com.epam.stv.locators.LocProjectLocators.SAUCELOGIN;
import static com.epam.stv.locators.LocProjectLocators.START_URL;

/**
 * Created by Tatiana_Sauchanka on 3/27/2017.
 */
public class RemoteLinuxTest {

    private static WebDriver driver;

    @BeforeClass
    public void setUp() throws MalformedURLException {

        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability("platform", "Linux");
        capabilities.setCapability("version", "48.0");

        driver = new RemoteWebDriver(
                new URL("http://" + SAUCELOGIN + ":" +
                        ACCESS_KEY + "@ondemand.saucelabs.com:80/wd/hub"),
                capabilities);
    }
//        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
//        capabilities.setPlatform(Platform.WINDOWS);
//        try {
//            driver = new RemoteWebDriver(new 	URL("http://localhost:4444/wd/hub"), capabilities);
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }

    @BeforeClass(description = "Start browser")
    public void startBrowser() {
        driver.get(START_URL);
    }

    @BeforeClass(dependsOnMethods = "startBrowser", description = "Add implicit wait and maximize window")
    public void addImplicitly() {
        // set a certain implicit wait timeout
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        // Maximize browser window
        driver.manage().window().maximize();
    }

    @Test
    public void clickOnWiggleIcon() {
        new WiggleSignInFactoryPage(driver).clickOnOrangeWiggleIcon();
    }

    @Test (dependsOnMethods = {"clickOnWiggleIcon"})
    public void assertSignLink() {
        boolean b = new WiggleMainFactoryPage(driver).isSignLinkDisplayed();
        Assert.assertEquals(b, true);
    }



}
