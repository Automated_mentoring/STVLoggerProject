package com.epam.stv.staticfactorymethod;

/**
 * Created by Tatiana_Sauchanka on 4/14/2017.
 */
public class DeliveryPayment {
    private String langId = null;
    private String currency = null;
    private String deliveryDestination = null;

    public String getLangId() {
        return langId;
    }

    public void setLangId(String langId) {
        this.langId = langId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDeliveryDestination() {
        return deliveryDestination;
    }

    public void setDeliveryDestination(String deliveryDestination) {
        this.deliveryDestination = deliveryDestination;
    }

















}
