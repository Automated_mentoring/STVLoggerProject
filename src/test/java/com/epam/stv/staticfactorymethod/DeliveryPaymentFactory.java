package com.epam.stv.staticfactorymethod;

/**
 * Created by Tatiana_Sauchanka on 4/14/2017.
 */
public class DeliveryPaymentFactory {

    public static DeliveryPayment createSimplePayment(){
        DeliveryPayment deliveryPayment = new DeliveryPayment();
        deliveryPayment.setCurrency("USD United States Dollars");
        deliveryPayment.setDeliveryDestination("USA");
        deliveryPayment.setLangId("US - English");
        return deliveryPayment;
    }

    public static DeliveryPayment createPaymentWithCurrency(String currency){
        DeliveryPayment deliveryPayment = createSimplePayment();
        deliveryPayment.setCurrency(currency);
        return deliveryPayment;
    }

    public static DeliveryPayment createPaymentWithDestination(String country){
        DeliveryPayment deliveryPayment = createSimplePayment();
        deliveryPayment.setDeliveryDestination(country);
        return deliveryPayment;
    }

    public static DeliveryPayment createRegionPayment(String region){
        DeliveryPayment deliveryPayment = createSimplePayment();
        deliveryPayment.setLangId(region);
        return deliveryPayment;
    }

}
