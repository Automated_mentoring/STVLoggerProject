package com.epam.stv.singlenton;


import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static com.epam.stv.locators.LocProjectLocators.START_URL;
import static com.epam.stv.locators.LocProjectLocators.WIGGLE_ICON_LOCATOR;

/**
 * Created by Tatiana_Sauchanka on 4/11/2017.
 */
public class BasicWebDriverSinglentonTest {

    @Test(description = "Start browser")
    public void setUp() {
        WebDriverSingleton.getWebDriverInstance().get(START_URL);
        (new WebDriverWait(WebDriverSingleton.getWebDriverInstance(), 5)).
                until(new ExpectedCondition<WebElement>() {
                    public WebElement apply(WebDriver webDriver) {
                        return webDriver.findElement(WIGGLE_ICON_LOCATOR);

                    }
                });
    }

    @Test(dependsOnMethods = "setUp", description = "Add implicite wait and maximize window")
    public void addImplicitly() {
        WebDriverSingleton.getWebDriverInstance().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        WebDriverSingleton.getWebDriverInstance().manage().window().maximize();
    }

    @AfterClass
    public void afterClass() throws Exception {
        WebDriverSingleton.getWebDriverInstance().quit();
    }


}
