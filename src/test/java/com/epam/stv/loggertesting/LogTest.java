package com.epam.stv.loggertesting;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

/**
 * Created by Tatiana_Sauchanka on 4/17/2017.
 */
public class LogTest {

    private static Logger log = Logger.getLogger(LogTest.class);

    @Test
    public void logTestInfo(){
        log.info("This is info log");
    }

    @Test
    public void logTestError(){
        log.error("This is an error");
    }

}
