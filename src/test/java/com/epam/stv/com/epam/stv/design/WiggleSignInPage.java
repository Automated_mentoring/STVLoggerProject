package com.epam.stv.com.epam.stv.design;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static com.epam.stv.locators.LocProjectLocators.WIGGLE_ICON_LOCATOR;
/**
 * Created by Tatiana_Sauchanka on 3/6/2017.
 */
public class WiggleSignInPage extends Page {

    private final By EMAIL_ADDRESS_LOCATOR = By.xpath("//input[@name='LogOnModel.UserName']");
    private final By PASSWORD_LOCATOR = By.xpath("//input[@name='LogOnModel.Password']");
    private final By LOGIN_BUTTON_LOCATOR = By.id("qa-login");
    private final By WARNING_MESSAGE_LOCATOR = By.cssSelector(".bem-alert--danger");
    private final By SHOW_PASSWORD_LOCATOR = By.cssSelector("#passwordToggle");


    public WiggleSignInPage (WebDriver driver){
        super(driver);
    }

    public WiggleSignInPage inputEmailAddress(String userEmail) {
        WebElement emailAddress = driver.findElement(EMAIL_ADDRESS_LOCATOR);
        emailAddress.isDisplayed();
        emailAddress.sendKeys(userEmail);
        return this;
    }

    public WiggleSignInPage clickOnShowPassword() {
        WebElement showPassword = driver.findElement(SHOW_PASSWORD_LOCATOR);
        showPassword.click();
        return this;
    }

    public WiggleSignInPage inputPassword(String password) {
        WebElement passwordElement = driver.findElement(PASSWORD_LOCATOR);
        passwordElement.isDisplayed();
        passwordElement.sendKeys(password);
        return this;
    }

    public WiggleMainPage clickLoginButton() {
        WebElement loginButton = driver.findElement(LOGIN_BUTTON_LOCATOR );
        loginButton.isDisplayed();
        loginButton.click();
        return new WiggleMainPage(driver);
    }

    public WiggleMainPage clickOnOrangeWiggleIcon() {
        System.out.println("Navigate to main page");
        driver.findElement(WIGGLE_ICON_LOCATOR).click();
        return new WiggleMainPage(driver);
    }

    public boolean isEmailAddressFieldDisplayed(){
        WebElement emailAddressField = driver.findElement(EMAIL_ADDRESS_LOCATOR);
        return emailAddressField.isDisplayed();
    }

    public boolean isWiggleIconDisplayed(){
        WebElement orangeWiggleIcon = driver.findElement(WIGGLE_ICON_LOCATOR);
        return orangeWiggleIcon.isDisplayed();
    }

    public boolean isLoginButtonDisplayed(){
        WebElement loginButton = driver.findElement(LOGIN_BUTTON_LOCATOR);
        return loginButton.isDisplayed();
    }

    public boolean isWarningMessageDisplayed(){
        WebElement warningMessage = driver.findElement(WARNING_MESSAGE_LOCATOR);
        return warningMessage.isDisplayed();
    }

    public String warningMessageText() {
        WebElement warningMessage = driver.findElement(WARNING_MESSAGE_LOCATOR);
        String warningText = warningMessage.getText().substring(0, 30);
        return warningText;
    }

    public String lableText() {
        WebElement lableLogin = driver.findElement(LOGIN_BUTTON_LOCATOR);
        String loginText = lableLogin.getText();
        return loginText;
    }
}
