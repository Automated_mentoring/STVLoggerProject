package com.epam.stv.com.epam.stv.design;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by Tatiana_Sauchanka on 3/6/2017.
 */
public class WiggleMainPage extends Page{

    private final By REGISTER_LINK_LOCATOR = By.id("btnJoinLink");
    private final By SIGN_LINK_LOCATOR = By.cssSelector("#btnSignIn");

    public WiggleMainPage (WebDriver driver){
        super(driver);
    }

//    public void open() {
//        getDriver();
//    }

    public WiggleSignInPage clickOnSignLink() {
        System.out.println("Go to Sign in page");
        WebElement signLink = driver.findElement(SIGN_LINK_LOCATOR);
        signLink.click();
        return new WiggleSignInPage(driver);
    }

    public boolean isRegisterLinkDisplayed(){
        WebElement registerLink = driver.findElement(REGISTER_LINK_LOCATOR);
        return registerLink.isDisplayed();
    }

    public boolean isSignLinkDisplayed(){
        WebElement signLink = driver.findElement(SIGN_LINK_LOCATOR);
        return signLink.isDisplayed();
    }

}
