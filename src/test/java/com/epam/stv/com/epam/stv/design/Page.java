package com.epam.stv.com.epam.stv.design;

import org.openqa.selenium.WebDriver;

/**
 * Created by Tatiana_Sauchanka on 3/7/2017.
 */
public abstract class Page {

    protected WebDriver driver;

    public Page (WebDriver driver){
        this.driver = driver;
    }

    public WebDriver getDriver()
    {
        String exePath = "C:\\Chromedriver\\chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", exePath);
        return this.driver;
    }



}
